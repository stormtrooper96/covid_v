let  url='https://e.infogram.com/api/live/flex/0e44ab71-9a20-43ab-89b3-0e73c594668f/832a1373-0724-4182-a188-b958f9bf0906?'
var fechas=[]
var cuenta=[]

var estado=""

fetch(url)
.then(res => res.json())
.then((out) => {
  var d= new Date(out.refreshed)
var datos=out.data[0]
const casos=[]

for (i in datos){

if(datos[i][4]=="Recuperado" ) estado="recuperado"
else if(datos[i][4]=="Fallecido" ) estado="fallecido"
else estado= "activo"

caso ={"fecha":datos[i][1],
    "departamento ":datos[i][3],
    "ciudad ":datos[i][2],
    "numero":1,
    "recuperacion":datos[i][4],
    "estado":estado,
    "tipo":datos[i][7],
    "pais_origen":datos[i][8],
    "sexo":datos[i][6],
    "acumulado":datos[i][0]

}
fechas.push(datos[i][1])
casos.push(caso)
cuenta.push(1)
    
}
casos.shift()
fechas.shift()
cuenta.shift()
var x=casos.length
var fecha =d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear();
plotcounter("Casos totales:"+fecha,x,'casos')
barchar(fechas,cuenta,"casosdia","Casos por dia")
getActiveCases(casos)
getNewCases(casos)
deaths(casos)
})
.catch(err => { throw err });


function barchar(xa,ya,divis,titulo){
    var data = [
        {
          x: xa,
          y: ya,
          title: { text: titulo},
          type: 'bar',
          transforms: [{
            type: 'aggregate',
            groups: xa,
            aggregations: [
              {target: 'y', func: 'sum', enabled: true},
            ]
          }]
          
          
        }
        
      ];
      
      Plotly.newPlot(divis, data);

}

function plotcounter(titulo,valor,divi){
    var data = [
        {
          domain: { x: [0, 1], y: [0, 1] },
          value: valor,
          title: { text: titulo},
          type: "indicator",
          mode: "gauge+number",

          delta: { reference: 400 },
          
          gauge: { axis: { range: [null, valor*2] } },
 
        }
        

      ];
      
      var layout = { width: 600, height: 400};
      Plotly.newPlot(divi, data, layout);

}
function getActiveCases(data){
    x=0
    for (i in data){
     if(data[i].estado=="activo") x+=1
     
    } 
    plotcounter("Casos activos",x,"activos")


}
function deaths(data){
    x=0
    for (i in data){
     if(data[i].estado=="fallecido") x+=1
     
    } 
    plotcounter("Fallecidos",x,"fallecidos")

}

getdatabycity

function getNewCases(data){
    base=0
    diario=0

    var actual=new Date()
    var yesterday=new Date()
    yesterday.setDate( yesterday.getDate() - 1 );
    actual=actual.getDate()+'/'+(actual.getMonth()+1)+'/'+actual.getFullYear();
    yesterday=yesterday.getDate()+'/'+(yesterday.getMonth()+1)+'/'+yesterday.getFullYear();
    
    for (i in data){
     if(data[i].fecha!=actual) base+=1
     else if (data[i].fecha==actual) diario+=1
    } 
    diario=diario+base
 
    plotadd(diario,base,"nuevos","Nuevos casos")


}

function plotadd(cuenta,base,divi,titulo){

    var data = [
        {
          type: "indicator",
          mode: "number+delta",
          title: { text: titulo},
          value: cuenta,
          number: { prefix: "+" },
          delta: { position: "top", reference: base },
          domain: { x: [0, 1], y: [0, 1] }
        }
      ];
      
      var layout = {
        paper_bgcolor: "lightgray",
        width: 600,
        height: 400,
        margin: { t: 0, b: 0, l: 0, r: 0 }
      };
      
      Plotly.newPlot(divi, data, layout);
}

